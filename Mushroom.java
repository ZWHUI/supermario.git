package enery;

import java.awt.*;

import javax.swing.ImageIcon;

public class Mushroom {
	public int x,y;//蘑菇的坐标
    public double doublex;
    private int ltop, rtop;//蘑菇需要碰撞检测
    public int width=30,height=32;//蘑菇的大小
    private double vx = 0.5;    //行走速度
    public Image img;
    private Image img1 = new ImageIcon("image/fungus1.png").getImage();
    private Image img2 = new ImageIcon("image/fungus2.png").getImage();
    public void changeImg(){
        if (img==img1){
            img = img2;
        }else if(img==img2) {
            img = img1;
        }
    }
    public Mushroom(int x, int y, int width, int height, Image img) {
        this.x = x;
        this.y = 358;
        this.doublex = x;
        this.ltop = x-450;
        this.rtop = x-100;
        this.width = width;
        this.height = height;
        this.img=img;
    }
    public void move(){
        if (x<ltop){//往右走
            vx =  0.5;
        }
        if (x>rtop){//往左走
            vx = -0.5;
        }
        doublex += vx;
        this.x = (int)doublex;
    }
    
    
}
