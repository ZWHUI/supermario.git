package map;

import java.awt.Image;
import java.util.ArrayList;


public class Brick extends Obstacle{

	public boolean hit = false;

	public void setHit(boolean hit) {
		this.hit = hit;
	}

	public boolean isHit() {
		return hit;
	}

	public int minY = y - 10;

	public int maxY = y;

	public int vY = 1;

	public void move(){
		if (y<minY)
			vY = -1;
		y -= vY;
		if (y>=maxY){
			hit = false;
			vY =  1;
		}
	}
	public boolean hascoin(ArrayList<Coin> coinList){
		for (Coin coin:coinList)
			if(coin.equal(this.x, 232)==true) 
				return true;
		return false;
		
	}
	public Brick(int x, int y, int width, int height, Image img) {
		super(x, y, width, height, img);
	}

}
