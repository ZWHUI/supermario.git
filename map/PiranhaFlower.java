package map;

import javax.swing.*;
import java.awt.*;

public class PiranhaFlower{
    public int x,y;
    public double doubleY;
    private int top, bot;
    public int width,height;
    private double vy = 0.5;    
    public Image img;
    private Image img1 = new ImageIcon("image/flower1.1.png").getImage();
    private Image img2 = new ImageIcon("image/flower1.2.png").getImage();;

    int i;
	public void changeImg() {
		i++ ;
		if(i<25)
			return ;
		else
			i = 1;
		if(img == img1)
			img = img2;
		else
			img = img1;
	}


    public PiranhaFlower(int x, int y, int width, int height, Image img) {
        this.x = x;
        this.y = y;
        this.doubleY = y;
        this.top = y - height;
        this.bot = y;
        this.width = width;
        this.height = height;
        this.img = img;
    }
    public void move(){
        if (y<top){
            vy =  0.5;
        }
        if (y>bot){
            vy = -0.5;
        }
        doubleY += vy;
        this.y = (int)doubleY;
    }
}
