package map;

import java.awt.Image;

public class Castle extends Obstacle {

	public Castle(int x, int y, int width, int height, Image img) {
		super(x, y, width, height, img);
	}
}