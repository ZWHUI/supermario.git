package map;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import enery.Enery;
import enery.PiranhaFlower;
import mario.KeyListener;
import mario.Mario;
import util.LoopMusicPlayer;
import util.MusicPlayer;
import enery.mushroom;
import enery.tortoise;


public class GameFrame extends JFrame{
	
	public Mario mario;
	public Enery enery;
	public Grass grass;
	public Obstacle pipe,brick,castle,box;
	public White white;
	public Coin coin,fcoin;
	public PiranhaFlower flower;
	//背景图片
	public BackgroundImage bg ;
	public int coinnum=0,mrnum=0;
	public ArrayList<Obstacle> obstacleList = new ArrayList<Obstacle>();
	public ArrayList<White> whiteList = new ArrayList<White>();
	public ArrayList<Enery> eneryList = new ArrayList<Enery>();
	public ArrayList<Grass> grassList = new ArrayList<Grass>();
	public ArrayList<Coin> coinList = new ArrayList<Coin>();
	public ArrayList<Mushroom> mrList = new ArrayList<Mushroom>();
	public ArrayList<PiranhaFlower> flowerList = new ArrayList<PiranhaFlower >();
	public ArrayList<tortoise> tortoiseList = new ArrayList<>();
    public ArrayList<mushroom> mushList = new ArrayList<>();
    public mushroom mushroom;
	public tortoise tortoise;

	public Mushroom mr;
	boolean mrb=false;
	public int [][] map =null;

	public Mushroom getMushroom() {
		return mr;
	}
	public ArrayList<mushroom> getmushList() {//返回蘑菇数组
		return mushList;
	}
	public ArrayList<tortoise> gettortoiseList() {//返回乌龟shuzu
		return tortoiseList;
	}

	public boolean getMrbolean() {
		return mrb;
	}
	public ArrayList<PiranhaFlower> getFlowerList() {
		return flowerList;
	}
	public ArrayList<White> getwhiteList() {
		return whiteList;
	}
	public ArrayList<Coin> getCoinList(){
		return coinList;
	}
	//构造函数里面初始化背景图片和马里奥对象
	public GameFrame() throws Exception {
		
		mario = new Mario(this);
		mario.start();
		mr=new Mushroom(this);
		Map mp= new Map();
		bg = new BackgroundImage();
		Thread bgm = new Thread(new LoopMusicPlayer("audio\\bgm.mp3"));
		bgm.start();
		//窗体重绘线程
		new Thread(){
			public void run(){
				while(true){
					//重绘窗体
					repaint();
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
		
		map=mp.readMap();
		//读取地图，并配置地图
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				//读取到的是1，画砖头
				if(map[i][j]==1){
					brick = new Brick(j*30,i*30,30,30,new ImageIcon("image/brick.png").getImage());
					obstacleList.add(brick);
				}
				if(map[i][j]==13){
					box = new Ebox(j*30,i*30,30,30,new ImageIcon("image/sbrick.png").getImage());
					obstacleList.add(box);
				}
				//读到21画金币
				if(map[i][j]==2){
					box = new Mbox(j*30,i*30,30,30,new ImageIcon("image/coin_brick.png").getImage());
					obstacleList.add(box);
				}
				
				if(map[i][j]==21){
					Coin coin= new Coin(
							j*30, i*29, 30, 30, new ImageIcon("image/coin1.png").getImage());
					coinList.add(coin);
					
				}
				
				
				if(map[i][j]==3){
					PiranhaFlower flower = new PiranhaFlower(
							j*30+15, i*30+20, 30, 63, new ImageIcon("image/flower1.1.png").getImage());
					flowerList.add(flower);
					
					pipe = new Pipe(j*30,i*30,60,130,new ImageIcon("image/pipe.png").getImage());
					obstacleList.add(pipe);
				}
				
				if(map[i][j]==30){
					pipe = new Pipe(j*30,i*30,60,100,new ImageIcon("image/pipe.png").getImage());
					obstacleList.add(pipe);
				}
				
				if(map[i][j]==31){
					
					PiranhaFlower flower = new PiranhaFlower(
							j*30+15, i*30+20, 30, 63, new ImageIcon("image/flower1.1.png").getImage());
					flowerList.add(flower);
					
					pipe = new Pipe(j*30,i*30,60,100,new ImageIcon("image/pipe1.png").getImage());
					obstacleList.add(pipe);
				}
				if(map[i][j]==32){
					
					pipe = new Pipe(j*30,i*30,60,100,new ImageIcon("image/pipe1.png").getImage());
					obstacleList.add(pipe);
				}
				
				if(map[i][j]==4){
					white = new White(j*30,i*30,31,31,new ImageIcon("image/white.png").getImage());
					whiteList.add(white);
					
					grass = new Grass(j*30-5,i*30,36,31,new ImageIcon("newimage/1.png").getImage());
					grassList.add(grass);
				}
				if(map[i][j]==5){
					grass = new Grass(j*30,i*30,31,31,new ImageIcon("newimage/2.png").getImage());
					grassList.add(grass);
				}
				if(map[i][j]==6){
					grass = new Grass(j*30,i*30,31,31,new ImageIcon("newimage/3.png").getImage());
					grassList.add(grass);
				}
				if(map[i][j]==7){
					grass = new Grass(j*30,i*30,31,31,new ImageIcon("newimage/4.png").getImage());
					grassList.add(grass);
				}
				if(map[i][j]==8){
					grass = new Grass(j*30,i*30,30,30,new ImageIcon("newimage/grass_soil.png").getImage());
					grassList.add(grass);
				}
				
				if(map[i][j]==9){
					white = new White(j*30,i*30,31,31,new ImageIcon("image/white.png").getImage());
					whiteList.add(white);
					
					grass = new Grass(j*30,i*30,31,31,new ImageIcon("newimage/6.png").getImage());
					grassList.add(grass);
				}
				
				if(map[i][j]==10){
					grass = new Grass(j*30,i*30,31,31,new ImageIcon("newimage/grass_soil.png").getImage());
					grassList.add(grass);
				}
				
				if(map[i][j]==11){
					white = new White(j*30,i*30,31,31,new ImageIcon("image/white.png").getImage());
					whiteList.add(white);
				}
				
				if(map[i][j]==12){
					box = new Cbox(j*30,i*30,30,30,new ImageIcon("image/coin_brick.png").getImage());
					coinnum=0;
					obstacleList.add(box);
					
				}
				if(map[i][j]==99){
					castle =new Castle(j*30,i*30,180,160,new ImageIcon("image/castle.png").getImage());
					obstacleList.add(castle);
				}
				if(map[i][j]==15) {
					mushroom mush = new mushroom(j*30, i*30, 30, 32, new ImageIcon("image/fungus1.png").getImage());
					mushList.add(mush);
				}
				if(map[i][j]==16) {
					tortoise tor = new tortoise(j*30, i*30, 30, 32, new ImageIcon("image/Ltortoise1.png").getImage());
					tortoiseList.add(tor);
				}

			}
		}
	}
	
	public void initFrame(){
		//设置窗体相关属性
		this.setSize(800,450);
		this.setTitle("超级玛丽");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(3);
		this.setVisible(true);
		
		//该窗体添加键盘监听
		KeyListener kl = new KeyListener(this);
		this.addKeyListener(kl);
	}
	
	public void paint(Graphics g) {
		//利用双缓冲画背景图片和马里奥
		BufferedImage bi =(BufferedImage)this.createImage(this.getSize().width,this.getSize().height);
		Graphics big =bi.getGraphics();
		big.drawImage(bg.img, bg.x, bg.y, null);
		
		
		for (White white:whiteList){
			big.drawImage(white.img, white.x, white.y, white.width, white.height,null);//绘制金币
		}
		//画怪物
		for (PiranhaFlower flower:flowerList){
			flower.move();
			flower.changeImg();
			big.drawImage(flower.img, flower.x, flower.y,null);
		}
		for (int i = 0; i < obstacleList.size(); i++) {
			Obstacle obs =obstacleList.get(i);
			Mushroom mushroom=null;
			Obstacle obss;
			if (obs instanceof Brick && ((Brick) obs).isHit()){
					
				((Brick) obs).move();
					for (Coin coin:coinList){
						if(coin.equal(obs.x, obs.y-27)==true) {
							new Thread(new MusicPlayer("audio\\得到金币.mp3")).start();
							coin.x=0;
							coin.y=0;
							Mario.result++;
						}
					}
				
				
			}
			
			if (obs instanceof Mbox && ((Mbox) obs).isHit()&&mrnum<1){
				((Mbox) obs).move();
				mrb=true;
				mushroom=new Mushroom(obs.x, obs.y-30, 30, 30, new ImageIcon("image/mushroom.png").getImage()); 
				mr=mushroom;
				mrnum+=1;
			}
			
			if (obs instanceof Cbox && ((Cbox) obs).isHit()&&coinnum<3){
				((Cbox) obs).move();
				Coin coin= new Coin(obs.x, obs.y-40, 30, 30,  new ImageIcon("image/coin1.png").getImage());
				if(obs.y==109) {
					new Thread(new MusicPlayer("audio\\得到金币.mp3")).start();
					coinnum+=1;
					Mario.result++;
				}	
					big.drawImage(coin.img, coin.x, coin.y, coin.width, coin.height,null);//绘制金币
			}
			if(coinnum>=3&&obs instanceof Cbox) {
				Ebox ebox=new Ebox(obs.x,obs.y,30,30,new ImageIcon("image/coin_brick_null.png").getImage());
				big.drawImage(ebox.img, ebox.x, ebox.y, ebox.width, ebox.height,null);
			}
			else if(obs instanceof Mbox&&mrnum>=1) {
				Ebox ebox=new Ebox(obs.x,obs.y,30,30,new ImageIcon("image/coin_brick_null.png").getImage());
				big.drawImage(ebox.img, ebox.x, ebox.y, ebox.width, ebox.height,null);
			}
			else
				big.drawImage(obs.img, obs.x, obs.y, obs.width, obs.height,null);//绘制当前可用的指定图像的大小。
		}

		for (Coin coin:coinList){
			coin.changeImg();
			//System.out.println("金币x："+coin.x+"金币y："+coin.y);
			big.drawImage(coin.img, coin.x, coin.y, coin.width, coin.height,null);//绘制金币
		}
		//绘制草地
		for (Grass grass :grassList){			
			big.drawImage(grass.img, grass.x, grass.y, grass.width, grass.height,null);
		}
		
		//产生蘑菇
		if(mrb==true) {
			big.drawImage(mr.img, mr.x, mr.y, mr.width, mr.height,null);//绘制金币
			mr.move();
		}
		for(mushroom mush:mushList) {
			if(mush.doublex!=0.0) {
				mush.move();
				mush.changeImg();
			}
				
			big.drawImage(mush.img,mush.x,mush.y,null);
		}
		//画乌龟
		for(tortoise tor:tortoiseList) {
			if(tor.doublex!=0.0) {
				tor.move();
				tor.changeImg();
			}
			
			big.drawImage(tor.img,tor.x,tor.y,null);
		}

		//画人物
		big.drawImage(mario.img, mario.x, mario.y, mario.width, mario.height,null);
		g.drawImage(bi,0,0,null);	
		
		g.drawString("分数"+Mario.result,30,80);
	}
	
	
}
