package map;

import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

public class Mushroom  {
	  public int x,y;
	  public int width,height;    
	  public int vx=1,vy=2;
	  public double changex,changey;
	  public Image img;
	  public boolean hit=false;
	  GameFrame gf;
	  public Mushroom  (GameFrame gf) {
			this.gf=gf;
		
		}
		

	public  Mushroom (int x, int y, int width, int height, Image img) {
	        this.x = x;
	        this.y = y;
	        this.width = width;
	        this.height = height;
	        this.img = img;
	    }
	  
		public void move(){
				changex=x;
				changey=y;
				if(this.x>=480&&this.y<=360) {
					changey+=vy;
					this.y=(int)changey;
				}
				if(this.x<=665&&hit==false) {
					if(this.x==665)
						hit=true;
					changex+=vx;
			        this.x =(int)changex;
				}
				else {
					changex-=vx;
			        this.x =(int)changex;
				}
		        
		    }
	
}
